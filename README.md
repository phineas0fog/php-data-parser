# Php Data Parser
A parser for arrays of objects for *PHP* with
*MySQL* like syntax.
 
## About
On my different projects, I need to query arrays of objects
and for this, I repeat the same code structures - with `foreach` for example - many times and it's not *DRY*...

So, I decided to design a system who allows to query data from array with the same syntax as *MySQL*. It's possible to `SELECT` or `DELETE`, etc.

### Example
Assume this code :
```php
class Person {
    public $name;
    public $age;
}
$alice = new Person("Alice", 42);
$bob = new Persom("Bob", 42);

$a = [$alice, $bob]; // array of persons
// syntax : dp::exec(<query>, <case_insensitive>);
$res = dp::exec("SELECT * FROM $a WHERE 'name' = 'alice'", true);
// $res => [$alice];

$res = dp::exec("SELECT * FROM $a WHERE 'age' = 42");
// $res => [$alice, $bob];
```
